## lukujonoja

### Aritmeettinen lukujono

#### Jono

$$a_1,~a_1+d,~a_1+2d,~...$$

#### Yleinen jäsen

$$a_n=a_k+(n-k)d$$

#### Summa

$$\sum_{i=k}^n a_i=(n-k+1)\cdot\frac{a_k+a_n}{2}$$

#### Sarja

suppenee vain jos kaikki jäsenet ovat $0$, jolloin suppenee nollaan.

### Geometrinen lukujono

#### Jono

$$a_1,~a_1q,~a_1q^2,~...$$

#### Yleinen jäsen

$$a_n=a_kq^{n-k}$$

#### Summa

$$\sum_{i=1}^n a_i=a_1\cdot\frac{1-q^{n}}{1-q},~q\neq1$$
$$\sum_{i=1}^n a_i=na_1,~q=1$$

#### Sarja

$$\sum_{i=1}^\infty a_i=\frac{a_1}{1-q},~\lvert q \rvert<1$$

