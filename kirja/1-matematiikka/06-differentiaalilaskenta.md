## Differentiaalilaskenta

### Derivaatan määritelmä

$f'(x)=\lim_{h\rightarrow0} \frac{f(x+h)-f(x)}{h}$

### Derivoimissääntöjä

$D(k)=0$

$D(kf(x))=kf'(x)$

$D(f(x)+g(x))=f'(x)+g'(x)$

$D(f(x)g(x))=f(x)g'(x)+f'(x)g(x)$

$D\left(\frac{f(x)}{g(x)}\right)=\frac{gf'(x)-fg'(x)}{g(x)^2}$

$D(g(f(x)))=g'(f(x))f'(x)$

### Derivoimiskaavoja

$D(x^n)=nx^{n-1}$

$D(\sin x)=\cos x$

$D(\cos x)=-\sin x$

$D(e^x)=e^x$

$D(a^x)=a^x \ln a$

$D(\ln \lvert x \rvert)=\ln x$

### Numeerinen derivointi

$$f'(a)=\frac{f(a-d)+f(a+d)}{2d}$$

jossa $d$ on kohtalaisen pieni luku 
käytetystä laskentatarkkuudesta riippuen. Yleensä $0,01$ on hyvä kokoluokka.
