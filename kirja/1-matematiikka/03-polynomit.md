## Polynomit

### Yleinen polynomi 

$$a_nx^n+a_{n-1}x^{n-1}+\cdots+a_2x^2+a_1x+a_0=\sum_{i=0}^na_ix^i$$

### Binomikaavat

$$(a+b)^2=a^2+2ab+b^2$$
$$(a-b)^2=a^2-2ab+b^2$$
$$(a+b)(a-b)=a^2-b^2$$
$$(a+b)^n=\sum_{i=0}^n{n\choose i}a^{n-i}b^i$$

### 2. asteen polynomi

$$ax^2+bx+c,~a\neq0$$

juurten summa $x_0+x_1=-\frac{a}{b}\Leftrightarrow x_0=\frac{a}{b}-x_1$

juurten tulo $x_0x_1=\frac{c}{a}\Leftrightarrow x_0=\frac{c}{x_1a}$

#### 2. asteen yhtälön ratkaisukaava

$$x=\frac{-b\pm\sqrt{D}}{2a},~a\neq0$$
Kaavassa diskriminatti $D=b^2-4ac$. Reaaliluvuilla 2. asteen yhtälöllä on 0–2
juurita riippuen $D$:n arvosta. Jos $D>0$ on juuria 2, jos $D=0$ juuria
on 1 ja jos $D<0$ juuria on 0.

numeerisesti kestävässä kaavassa, jota kuuluu käyttää kun yhtälöitä ratkotaan
tietokoneella, käytetään $\sqrt{D}$:lle samaa merkkiä kuin
$-b$:llä on, jolloin toinen juuri voidaan ratkaista juurien summan tai tulon
kaavalla.

### TODO Joidenkin funktioiden polynomilaajennuksia

$$e^x=\sum_{i=0}^\infty \frac{x^i}{i!}$$
$$\ln{1+x}=\sum_{i=0}^\infty(-1)^i\frac{x^{2i+1}}{2i+1},~-1\lt x\le1 $$


### TODO Taylorin sarja 
