# Vapaat Taulukot

> Taulukkokirja on ihmisoikeus, joten sen pitäisi olla vapaasti saatavissa ja yhteisesti kehitettävissä.

## Riippuvuudet

Kirja on kirjoitettu pandoc-tyylisessä markdown:ssa johon on sisällytetty latex-kaavoja,
joten sen varmaan kääntämiseen tarvitsee [pandoc:n](https://pandoc.org/) ja pdflatex:n
